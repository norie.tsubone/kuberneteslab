resource "aws_eks_cluster" "k8slab-eks" {
  name     = "k8slab-eks"
  role_arn = aws_iam_role.k8slab-eks.arn

  vpc_config {
    subnet_ids = [aws_subnet.public_subnet.id, aws_subnet.public_subnet-b.id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.k8slab-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.k8slab-AmazonEKSServicePolicy,
  ]
}

resource "aws_iam_role" "k8slab-eks" {
  name = "k8lab-eks"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "eks.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      },
    ]
  })
}

resource "aws_security_group" "k8slab" {
  vpc_id = aws_vpc.main.id
  name = "k8slab"
}

resource "aws_vpc_security_group_ingress_rule" "allow_master" {
  ip_protocol = "-1"
  referenced_security_group_id = aws_security_group.master
  security_group_id = aws_security_group.k8slab.id
}


resource "aws_iam_role_policy_attachment" "k8slab-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.k8slab-eks.name
}

resource "aws_iam_role_policy_attachment" "k8slab-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.k8slab-eks.name
}

output "cluster_id" {
  value = aws_eks_cluster.k8slab-eks.id
}
