
resource "aws_iam_role" "ec2_role" {
  name = "ec2_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      },
    ]
  })
}

resource "aws_iam_policy" "eks_full_access" {
  name        = "eks_full_access_policy"
  path        = "/"
  description = "EKS k8lab-eks cluster full access policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "eks:*",
        Resource = "arn:aws:eks:*:*:cluster/k8slab-eks"
      },
      {
        Effect = "Allow",
        Action = [
          "ec2:Describe*",
          "elasticloadbalancing:Describe*",
          "ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetRepositoryPolicy",
          "ecr:DescribeRepositories",
          "ecr:ListImages",
          "ecr:BatchGetImage",
          "autoscaling:DescribeAutoScalingGroups",
          "autoscaling:UpdateAutoScalingGroup"
        ],
        Resource = "*"
      }
    ]
  })
}

# AmazonSSMRoleForInstanceQuickSetupポリシーのアタッチメント
resource "aws_iam_role_policy_attachment" "s3" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "ssm" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}


# EKSフルアクセスポリシーのアタッチメント
resource "aws_iam_role_policy_attachment" "eks_full_access" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.eks_full_access.arn
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2_profile"
  role = aws_iam_role.ec2_role.name
}

resource "aws_security_group" "master" {
  vpc_id = aws_vpc.main.id
  name = "master"
}

resource "aws_vpc_security_group_egress_rule" "allow_all" {
  ip_protocol = "-1"
  cidr_ipv4 = "0.0.0.0/0"
  security_group_id = aws_security_group.master.id
}

resource "aws_instance" "master" {
  ami           = "ami-07c589821f2b353aa"
  instance_type = "t3a.small"
  subnet_id     = aws_subnet.public_subnet.id
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.id
  key_name = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [aws_security_group.master.id]

  root_block_device {
    volume_size = 20
  }
  tags = {
    Name = "master"
  }
}

output "master" {
  value = aws_instance.master.id
}
